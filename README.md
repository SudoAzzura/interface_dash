# Stage Caracterisation de signaux Radar
## Préparation

### Installation de Python

Ubuntu
* Install mini conda:  https://doc.ubuntu-fr.org/miniconda
* Download the latest shell script: `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`
* Make the miniconda installation script executable: `chmod +x Miniconda3-latest-Linux-x86_64.sh`
* Run miniconda installation script: `bash Miniconda3-latest-Linux-x86_64.sh -p $HOME/miniconda3`
* .bashrc has changed, you need to reload it: `source ~/.bashrc`

Windows
* Install mini conda:  https://docs.conda.io/en/latest/miniconda.html

### Environment 

* create new environment: `conda create --name my_env python=3.10`
* activate environment: `conda activate my_env`
* install libs: `conda install --force-reinstall -y -c conda-forge --file requirements.txt`

### Utilisation

* usage : python interface_dash.py 

### Interface Dash

![Alt text](./Int_Dach.png)
