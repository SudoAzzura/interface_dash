import dash
from dash import html
from dash import dcc
import dash_table
from dash.dependencies import Input, Output
from dash.dash_table.Format import Group
import plotly.graph_objects as go
from plotly.subplots import make_subplots

app = dash.Dash(__name__)

values1 = ['A Scores', 'B Scores']
x1 = [0, 1, 2, 3]
y1 = [95, 85, 75, 95]

values2 = ['X Scores', 'Y Scores']
x2 = [0, 1, 1, 3]
y2 = [195, 5, 75, 255]

app.layout = html.Div([
    dcc.Dropdown(
        id='dropdown',
        options=[
            {'label': 'Tableau 1', 'value': 'table1'},
            {'label': 'Tableau 2', 'value': 'table2'}
        ],
        value='table1'
    ),
    html.Div(id='output-graph'),
    html.Div(id='output-table')
])

@app.callback(
    Output('output-graph', 'children'),
    Output('output-table', 'children'),
    Input('dropdown', 'value')
)
def update_output(value):
    if value == 'table1':
        table = dash_table.DataTable(
            columns=[{'name': col, 'id': col} for col in values1],
            data=[dict(zip(values1, row)) for row in zip(x1, y1)]
        )
        scatter = dcc.Graph(
            figure={
                'data': [go.Scatter(x=x1, y=y1, mode='markers')],
                'layout': go.Layout(title='Graphe du Tableau 1')
            }
        )
        return scatter, table
    elif value == 'table2':
        table = dash_table.DataTable(
            columns=[{'name': col, 'id': col} for col in values2],
            data=[dict(zip(values2, row)) for row in zip(x2, y2)]
        )
        scatter = dcc.Graph(
            figure={
                'data': [go.Scatter(x=x2, y=y2, mode='markers')],
                'layout': go.Layout(title='Graphe du Tableau 2')
            }
        )
        return scatter, table

if __name__ == '__main__':
    app.run_server(debug=True)
